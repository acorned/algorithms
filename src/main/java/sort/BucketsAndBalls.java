package sort;

// Let's keep it simple: 0 is a red bucket, 1 is a green one, 2 is a white one, and 3 for empty bucket.

public class BucketsAndBalls {
    public static void sortBalls(int[] buckets) {

        int red = 0;
        while (buckets[red] == 0) {
            red++;
        }

        int empty = buckets.length - 1;
        while (buckets[empty] == 3) {
            empty--;
        }

        int white = empty - 1;
        while (buckets[white] == 2) {
            white--;
        }

        int green = red + 1;

        while (green < white) {
            while (buckets[green] != 1) {
                if (buckets[green] == 0) {
                    buckets[green] = buckets[red];
                    buckets[red] = 0;
                    red++;
                } else if (buckets[green] == 2) {
                    buckets[green] = buckets[white];
                    buckets[white] = 2;
                    white--;
                } else if (buckets[green] == 3) {
                    buckets[empty] = 3;
                    empty--;

                    buckets[green] = buckets[white];
                    buckets[white] = 2;
                    white--;
                }
            }
            green++;
        }
    }

    public static void main(String[] args) {
        int[] buckets = {0, 1, 2, 0, 1, 2, 3, 3, 2, 1, 0, 3, 0, 1, 0, 2};
        sortBalls(buckets);

        for (int i = 0; i < buckets.length; i++) {
            System.out.format("%d ", buckets[i]);
        }
        System.out.println();
    }
}
