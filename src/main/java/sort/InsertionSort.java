package sort;

public class InsertionSort {

    public static <T extends Comparable> void sort(T[] array) {
        for (int i = 0; i < array.length; i++) {
            T value = array[i];
            int index = i - 1;
            while (index >= 0 && value.compareTo(array[index]) < 0) {
                array[index + 1] = array[index];
                index--;
            }
            array[index + 1] = value;
        }
    }
}
