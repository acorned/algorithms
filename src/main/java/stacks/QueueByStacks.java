package stacks;

import java.util.Stack;

public class QueueByStacks<T> {

    private Stack<T> input = new Stack<>();
    private Stack<T> output = new Stack<>();

    public boolean isEmpty() {
        return input.empty() && output.empty();
    }

    public T push(T value) {
        while (!output.empty()) input.push(output.pop());
        input.push(value);
        return value;
    }

    public T pop() {
        while (!input.empty()) output.push(input.pop());
        return output.pop();
    }

    public T peek() {
        while (!input.empty()) output.push(input.pop());
        return output.peek();
    }
}
