package stacks;

import java.util.Stack;

public class BracesAnalyzer {
    public static boolean bracesAreCorrect(String s) {
        Stack<Character> braces = new Stack<>();
        for (Character c : s.toCharArray()) {
            if ("[({".indexOf(c) > -1) braces.push(c);
            if ("])}".indexOf(c) > -1 && pairBrace(c) != braces.pop()) return false;
        }
        if (!braces.empty()) return false;
        return true;
    }

    public static Character pairBrace(Character c) {
        return (c == '}') ? '{' :
                   (c == ']') ? '[' :
                       (c == ')') ? '(' : '0';
    }
}
