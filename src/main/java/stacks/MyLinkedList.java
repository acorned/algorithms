package stacks;

public class MyLinkedList<T> {
    private int size;
    private Node<T> first;

    public void add(T data) {

        Node<T> node = new Node<>(data);

        if (size == 0) {
            first = node;
            size++;
            return;
        }

        Node<T> temp = first;

        for (int i = 0; i < size - 1; i++) {
            temp = temp.next;
        }

        temp.next = node;
        size++;
    }

    public int size() {return size;}

    public T get(int index) {
        Node<T> node = first;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node.data;
    }

    private static class Node<T> {
        private T data;
        private Node<T> next;

        public Node(T data) {
            this.data = data;
        }
    }
}
