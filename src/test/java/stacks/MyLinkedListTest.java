package stacks;

import org.hamcrest.core.Is;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyLinkedListTest {

    @Test
    public void listIsFilledCorrect() {
        MyLinkedList<Integer> list = new MyLinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        assertThat(list.size(), Is.is(3));
        assertThat(list.get(2), Is.is(3));
    }

}