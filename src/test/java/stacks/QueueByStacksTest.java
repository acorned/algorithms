package stacks;

import org.hamcrest.core.Is;
import org.junit.Test;

import static org.junit.Assert.*;

public class QueueByStacksTest {

    @Test
    public void testQueue() {
        QueueByStacks<Integer> queue = new QueueByStacks<>();
        queue.push(1);
        queue.push(2);
        queue.push(3);
        queue.push(4);
        queue.push(5);

        for (int i = 1; i <= 5; i++) {
            assertThat(queue.pop(), Is.is(i));
        }
        assertTrue(queue.isEmpty());
    }

}