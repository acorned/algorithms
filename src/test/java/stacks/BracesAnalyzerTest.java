package stacks;

import org.junit.Test;

import static org.junit.Assert.*;

public class BracesAnalyzerTest {
    @Test
    public void bracesAreCorrect() {
        assertTrue(BracesAnalyzer.bracesAreCorrect("((([[[{{{}}}]]])))"));
        assertFalse(BracesAnalyzer.bracesAreCorrect("((([[[{{{}}}]]]))"));
    }

}