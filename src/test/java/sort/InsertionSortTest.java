package sort;

import org.hamcrest.core.Is;
import org.junit.Test;

import static org.junit.Assert.*;

public class InsertionSortTest {
    @Test
    public void sort() {
        String[] strings = {"asd", "aas", "fds", "bbb", "anr"};
        InsertionSort.sort(strings);
        String[] expected = {"aas", "anr", "asd", "bbb", "fds"};
        assertThat(strings, Is.is(expected));
    }
}