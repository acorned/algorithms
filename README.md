# Algorithms #

Exercises from algorithms and data structures course.

### What has done ###

* Simple linked list
* Function that checks if braces correct
* Queue made with two stacks
* Insertion sort
* Selection sort

### Contacts ###
<aleksandr_zheludkov@epam.com>